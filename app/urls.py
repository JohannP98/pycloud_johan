from django.urls import path, include
from . import views

urlpatterns = [
    path('login/', views.form_login, name='form_login'),
    path('crear_cuenta/', views.crear_cuenta, name='crear_cuenta'),
    path('forgot_password/', views.forgot_password, name='forgot_password'),
    path('404/', views.e404, name='e404'),
    path('login/auth', views.auth_login, name='auth_login'),
    path('logout/', views.auth_logout, name='auth_logout'),
    
    path('inicio/', views.inicio, name='inicio'),

    path('proyectos/', views.proyectos, name='proyectos'),
    path('proyectos/crear_proyecto', views.crear_proyecto, name='crear_proyecto'),
    path('peliculas/crear/', views.form_proyectos, name='form_proyectos'),
  
    path('tareas/', views.tareas, name='tareas'),   
    path('tareas/crear_tarea/', views.crear_tarea, name='crear_tarea'),

]