from django.db import models

class Proyecto(models.Model):
   
    nombre_proyecto = models.CharField(max_length=50)
    arquitecto = models.TextField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)

    class Meta: 
        app_label: 'app'


class Tarea(models.Model):

    titulo = models.CharField(max_length = 100 )
    desarrolladores = models.TextField()
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    activo = models.BooleanField()
    proyecto = models.ForeignKey(
        Proyecto, 
        related_name='tarea',
        on_delete = models.CASCADE,
        null = False
    )

    class Meta:
        app_label: 'app'

# Create your models here.

